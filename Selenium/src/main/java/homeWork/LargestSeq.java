package homeWork;

	import java.util.ArrayList;
	import java.util.HashSet;
	import java.util.List;
	import java.util.Set;

	public class LargestSeq {

	    public Integer[] getLargestSequence(Integer[] numberArray) {

	        List<Integer> largestSequence = new ArrayList<Integer>();
	        int largestSequenceCount = 0;

	        Set<Integer> numbersSet = getSetFromArray(numberArray);

	        for (Integer integer : numbersSet) {
	            if(numbersSet.contains(integer -1)) {
	                continue;
	            } else {
	                int count = 0;
	                List<Integer> largestSequenceTemp = new ArrayList<Integer>();
	                while(numbersSet.contains(integer)) {
	                    largestSequenceTemp.add(integer);
	                    integer++;
	                    count++;
	                }
	                if(count > largestSequenceCount) {
	                    largestSequenceCount = count;
	                    largestSequence = largestSequenceTemp;
	                }
	            }

	        }
	        return largestSequence.toArray(new Integer[largestSequence.size()]);
	    }

	    private Set<Integer> getSetFromArray(Integer[] numberArray) {
	        Set<Integer> numbersSet = new HashSet<Integer>();
	        if(null != numberArray && numberArray.length > 0) {
	            for (int number : numberArray) {
	                numbersSet.add(number);
	            }
	        }
	        return numbersSet;
	    }
	}

