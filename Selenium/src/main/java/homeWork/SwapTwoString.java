package homeWork;
import java.util.Scanner;
	 
	public class SwapTwoString 
	{  
	    public static void main(String[] args) 
	    {    
	        Scanner scr = new Scanner(System.in);
	         
	        System.out.println("Input The First String :");
	         
	        String str1 = scr.next();
	         
	        System.out.println("Input The Second String :");
	         
	        String str2 = scr.next();
	         
	        System.out.println("Before Swapping :");
	         
	        System.out.println("str1 : "+str1);
	         
	        System.out.println("str2 : "+str2);
	         
	        str1 = str1 + str2;
	         
	        str2 = str1.substring(0, str1.length()-str2.length());
	         
	        str1 = str1.substring(str2.length());
	         
	        System.out.println("After Swapping :");
	         
	        System.out.println("str1 : "+str1);
	         
	        System.out.println("str2 : "+str2);
	    }    
	}


