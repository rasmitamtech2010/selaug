	package week3.day1;
	import java.util.List;
	import java.util.concurrent.TimeUnit;

	import org.openqa.selenium.NoSuchElementException;
	import org.openqa.selenium.WebElement;
	import org.openqa.selenium.chrome.ChromeDriver;
	import org.openqa.selenium.support.ui.Select;

	public class leafTapsLogin1 {

		public static void main(String[] args) {
			// To open a chrome browser
					System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
					//Creating a obj for chrome driver
					ChromeDriver driver=new ChromeDriver();
			
					try {
			//Maximize the window
			driver.manage().window().maximize();
			//Implicit wait
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			//Loading URL 
			driver.get("http://leaftaps.com/opentaps/control/login");
			//Login
			
			try {
				driver.findElementById("username1").sendKeys("DemoSalesManager");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			driver.findElementById("password").sendKeys("crmsfa");
			driver.findElementByClassName("decorativeSubmit").click();
			//select CRM/SFA link
			driver.findElementByLinkText("CRM/SFA").click();
			//create lead and enter the details
			driver.findElementByLinkText("Create Lead").click();
			driver.findElementById("createLeadForm_companyName").sendKeys("Volante");
			String n1=driver.findElementById("createLeadForm_companyName").getAttribute("maxlength");
			System.out.println(n1);
			String n2=driver.findElementById("createLeadForm_companyName").getAttribute("name");
			System.out.println(n2);
			driver.findElementById("createLeadForm_firstName").sendKeys("Abirami");
			driver.findElementById("createLeadForm_firstName").sendKeys(" M");
			driver.findElementById("createLeadForm_lastName").sendKeys("Murugaiyan");
			driver.findElementById("createLeadForm_primaryPhoneCountryCode").clear();
			
			//For drop down box -->Find the drop down and store it in a web element
			WebElement src=driver.findElementById("createLeadForm_dataSourceId");
			//Create obj for select class to select a options form Drop down
			Select slct=new Select(src);
			//By using select class obj select the value with visible text
			slct.selectByVisibleText("Direct Mail");
			
			WebElement MC=driver.findElementById("createLeadForm_marketingCampaignId");
			Select slct1=new Select(MC);
			//Get all the options in a drop down and store it in a list
			List<WebElement> lMc=slct1.getOptions();//get options method return type is "List"
			int size=lMc.size();
			slct1.selectByIndex(size-1);
			driver.findElementByClassName("smallSubmit").click();
			
			}catch (NoSuchElementException e)
			{e.printStackTrace();
				System.out.println("No such Element exception");
			}
			
			finally{
				
				driver.close();
			}
			}
			

}
