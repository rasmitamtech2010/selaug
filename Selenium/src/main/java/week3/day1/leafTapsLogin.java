package week3.day1;

import java.util.List;

//import org.apache.http.impl.conn.tsccm.WaitingThread;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class leafTapsLogin {

	public static void main(String[] args) throws InterruptedException {
		// To open a chrome browser
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		//Creating a obj for chrome driver
		ChromeDriver driver=new ChromeDriver();
		//Maximize the window
		driver.manage().window().maximize();
		//Loading URL
		driver.get("http://leaftaps.com/opentaps/control/login");
		//Login
		driver.findElementById("username").sendKeys("DemoSalesManager");
		
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		//select CRM/SFA link
		driver.findElementByLinkText("CRM/SFA").click();
		//create lead and enter the details
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("Volante");
		driver.findElementById("createLeadForm_firstName").sendKeys("Abirami");
		
		driver.findElementById("createLeadForm_lastName").sendKeys("Murugaiyan");
		//For drop down box -->Find the drop down and store it in a web element
		WebElement src=driver.findElementById("createLeadForm_dataSourceId");
		//Create obj for select class to select a options form Drop down
		Select slct=new Select(src);
		//By using select class obj select the value with visible text
		slct.selectByVisibleText("Direct Mail");
		
		WebElement MC=driver.findElementById("createLeadForm_marketingCampaignId");
		Select slct1=new Select(MC);
		//Get all the options in a drop down and store it in a list
		List<WebElement> lMc=slct1.getOptions();//get options method return type is "List"
		int size=lMc.size();
		slct1.selectByIndex(size-1);
		driver.findElementByClassName("smallSubmit").click();
		
	}

}