package week3.day1;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class registerIrctc {

	public static void main(String[] args) throws InterruptedException 
	{
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		driver.findElementById("userRegistrationForm:userName").sendKeys("Rasmita");
		driver.findElementById("userRegistrationForm:password").sendKeys("Rasmita.mtech2010");
		driver.findElementById("userRegistrationForm:confpasword").sendKeys("Rasmita.mtech2010");
		WebElement src=  driver.findElementById("userRegistrationForm:securityQ");
		Select drop=new Select(src);
		drop.selectByVisibleText("Where did you first meet your spouse?");
		driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("Chennai");
		WebElement lang=  driver.findElementById("userRegistrationForm:prelan");
		Select drop1=new Select(lang);
		drop1.selectByVisibleText("English");
		driver.findElementById("userRegistrationForm:firstName").sendKeys("Rasmita");
		driver.findElementById("userRegistrationForm:gender:1").click();
		driver.findElementById("userRegistrationForm:maritalStatus:1").click();
		WebElement day =  driver.findElementById("userRegistrationForm:dobDay");
		Select drop3=new Select(day);
		drop3.selectByVisibleText("07");
		WebElement month =  driver.findElementById("userRegistrationForm:dobMonth");
		Select drop4=new Select(month);
		drop4.selectByVisibleText("APR");
		WebElement year =  driver.findElementById("userRegistrationForm:dateOfBirth");
		Select drop5=new Select(year);
		drop5.selectByVisibleText("1989");
		WebElement we =  driver.findElementById("userRegistrationForm:occupation");
		Select drop6=new Select(we);
		drop6.selectByVisibleText("Private");
		WebElement country =  driver.findElementById("userRegistrationForm:countries");
		Select drop7=new Select(country);
		drop7.selectByValue("94");
		driver.findElementById("userRegistrationForm:email").sendKeys("rasmita.mtech2010@gmail.com");
		//driver.findElementById("userRegistrationForm:isdCode").sendKeys("91",Keys.TAB);
		driver.findElementById("userRegistrationForm:mobile").sendKeys("9439377355");
		WebElement nationality =  driver.findElementById("userRegistrationForm:nationalityId");
		Select drop8=new Select(nationality);
		drop8.selectByVisibleText("India");
		driver.findElementById("userRegistrationForm:address").sendKeys("Chennai");
		driver.findElementById("userRegistrationForm:pincode").sendKeys("600061",Keys.TAB);
		Thread.sleep(1000);
		WebElement city =  driver.findElementById("userRegistrationForm:cityName");
		Select drop9=new Select(city);
		drop9.selectByIndex(1);
		Thread.sleep(1000);
		WebElement postoffice =  driver.findElementById("userRegistrationForm:postofficeName");
		Select drop10=new Select(postoffice);
		drop10.selectByIndex(5);
		driver.findElementById("userRegistrationForm:landline").sendKeys("22443809");
		driver.findElementById("userRegistrationForm:resAndOff:1").click();
		//Enter Office address
		WebElement country2 =  driver.findElementById("userRegistrationForm:countrieso");
		Select drop24=new Select(country2);
		drop24.selectByVisibleText("India");
		driver.findElementById("userRegistrationForm:addresso").sendKeys("Odisha");
		driver.findElementById("userRegistrationForm:pincodeo").sendKeys("760007",Keys.TAB);
		Thread.sleep(1000);
		WebElement city2 =  driver.findElementById("userRegistrationForm:cityNameo");
		Select drop22=new Select(city2);
		drop22.selectByIndex(1);
		
		Thread.sleep(1000);
		WebElement postoffice2 =  driver.findElementById("userRegistrationForm:postofficeNameo");
		Select drop23=new Select(postoffice2);
		drop23.selectByIndex(1);
		driver.findElementById("userRegistrationForm:landlineo").sendKeys("123456");

        //driver.close();

	}

}
