package week4.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class LearnAlert {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// To open a chrome browser
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		//Creating a obj for chrome driver
		ChromeDriver driver=new ChromeDriver();
		//Loading URL
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		//Implicit wait
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				
		//Maximize the window
		driver.manage().window().maximize();
		
		//Enter into the frame
		driver.switchTo().frame("iframeResult");
	
		//click on try it button
		driver.findElementById("//button[text()='Try it']").click();
		
	}

}
