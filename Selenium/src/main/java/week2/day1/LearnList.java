package week2.day1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LearnList {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<String> myPhone=new ArrayList<String>();
		myPhone.add("Nokia");
		myPhone.add("Samsung");
		myPhone.add("Iphone");
		myPhone.add("Samsung");		
		myPhone.add("moto");
		myPhone.remove("Iphone");
		int length=myPhone.size();
		System.out.println("Count of mobile phones : "+length);
		for(String eachPhone:myPhone) {
			System.out.println(eachPhone);
		}
		String firstPhone=myPhone.get(0);
		System.out.println("The first Phone is: "+firstPhone);
		System.out.println("The Last Phone is: "+myPhone.get(length-1));
		
		Collections.sort(myPhone);
		Collections.reverse(myPhone);
	}
}
