package wdMethods;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;



public class ProjectMethods extends SeMethods{
	@BeforeSuite(groups="common")
	public void beforeSuite() {
		
		beginResult();
	}
	@BeforeClass(groups="common")
	public void beforeClass() {
		startTestCase();
	}
	
	@Parameters({"url","usernmae","password"})
	@BeforeMethod(groups="common")
	public void login(String url,String username,String password) throws InterruptedException, IOException {
		startApp("chrome", url);
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, username);
		WebElement elePassword = locateElement("id","password");
		type(elePassword, password);
		WebElement eleLogin = locateElement("classname", "decorativeSubmit");
		click(eleLogin);
		Thread.sleep(3000);
		WebElement eleCRM = locateElement("linktext","CRM/SFA");
		click(eleCRM);
	}
	@AfterMethod(groups="common")
	public void closeApp() {
		closeBrowser();
	}
	@AfterSuite(groups="common")
	public void afterSuite() {
		endResult();
	}
	
	
	
	
	
	
	
	
}
