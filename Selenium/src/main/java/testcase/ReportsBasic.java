package testcase;


import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ReportsBasic {

	public static void main(String[] args) throws IOException {
		
		ExtentHtmlReporter html=new ExtentHtmlReporter("./Reports/basic_report.html");
		html.setAppendExisting(true);
		
		ExtentReports report=new ExtentReports();
		report.attachReporter(html);
		
		ExtentTest test=report.createTest("TC001_CreateLead","Create a new lead");
		test.assignCategory("Functional testing");
		test.assignAuthor("Abi");
		
		test.pass("Browser launched successfully",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
		test.pass("User nmae entered successfully",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img2.png").build());
		test.pass("Password entered successfully",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img3.png").build());
		test.pass("login button clicked");
		test.fail("Cannot create a lead");
		
		report.flush();
		
	}

}
