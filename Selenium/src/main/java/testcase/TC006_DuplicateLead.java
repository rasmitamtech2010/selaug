package testcase;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC006_DuplicateLead extends ProjectMethods{
	@BeforeTest(groups="sanity")
	public void beforeTest()
	{
		testCaseName="TC006_DuplicateLead";
		testCaseDesc="To Find duplicate lead";
		author="Abi";
		category="Smoke";
		System.out.println("before test");
	}
	@Test(groups="sanity")
	public void duplicateLead() throws Exception {
		
		click(locateElement("linktext", "Leads"));
		Thread.sleep(3000);

		//Duplicate lead
		click(locateElement("linktext", "Find Leads"));
		Thread.sleep(3000);
		click(locateElement("xpath", "//span[text()='Email']"));
		Thread.sleep(3000);
		type(locateElement("xpath", "//input[@name='emailAddress']"),"Abirami003@abi.net");
		click(locateElement("xpath", "//button[text()='Find Leads']"));
		Thread.sleep(3000);
		WebElement eleFirstName = locateElement("xpath","(//div[contains(@class,'x-grid3-col-firstName')])[1]");
		String text = getText(eleFirstName);
		click(eleFirstName);
		Thread.sleep(3000);
		click(locateElement("linktext", "Duplicate Lead"));
		verifyTitle("Duplicate Lead");
		click(locateElement("xpath","//input[@class='smallSubmit']"));
		Thread.sleep(3000);
		verifyExactText(locateElement("viewLead_firstName_sp"), text);
		
	}
}
