package testcase;


import java.io.IOException;

//import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;
//import wdMethods_old.SeMethods;

public class TC003_MregeLead extends ProjectMethods{
	
	@BeforeTest(groups="Regression")
	public void beforeTest()
	{
		testCaseName="TC003_Merge Lead";
		testCaseDesc="To Merge leads";
		author="Abi";
		category="function";
		System.out.println("before test");
	}
		@Test(groups="Regression")
		public void MergeLead() throws InterruptedException, IOException {
			
	//createReport("/./Reports/"+this.getClass().getName()+".html",this.getClass().getName(),"Create lead","Smoke","Abi");
	//1 Launch the browser
	startApp("Chrome", "http://leaftaps.com/opentaps/control/login");
	//2 Enter the username
	WebElement username = locateElement("username");
	
	type(username, "DemoSalesManager");
	//3 Enter the password
	WebElement password = locateElement("password");
	type(password, "crmsfa");
	//4 Click Login
	WebElement login = locateElement("classname","decorativeSubmit");
	click(login);
	
	//5 Click crm/sfa link
	WebElement crmsfa = locateElement("linktext","CRM/SFA");
	click(crmsfa);
	
	//6 Click Leads link
	WebElement Leads = locateElement("linktext","Leads");
	click(Leads);
	Thread.sleep(3000);
	
	//7 Click Merge leads
	WebElement MergeLeads = locateElement("linktext","Merge Leads");
	click(MergeLeads);
	Thread.sleep(3000);
	switchToWindow(0);
	
	//8 Click on Icon near From Lead
	WebElement From = locateElement("xpath","(//input[@id='ComboBox_partyIdFrom']//following::img)[1]");
	click(From);
	Thread.sleep(3000);
	//9 Move to new window
	switchToWindow(1);
	
	Thread.sleep(3000);
	//10 Enter Lead ID
	System.out.println("10 Enter Lead ID");
	WebElement LeadId = locateElement("xpath","//input[@name='id']");
	type(LeadId, "10");
	Thread.sleep(3000);
	
	//11 Click Find Leads button
	WebElement FindLeads = locateElement("xpath","//button[text()='Find Leads']");
	System.out.println(FindLeads);
	click(FindLeads);
	Thread.sleep(3000);
	switchToWindow(0);
			
	//12 Click First Resulting lead
	//13 Switch back to primary window
	//14 Click on Icon near To Lead
	WebElement Lead1 = locateElement("xpath","(//input[@id='ComboBox_partyIdFrom']//following::img)[2]");
	Lead1.click();
	
	
	
//15 Move to new window
	switchToWindow(1);
	
	//16 Enter Lead ID
	//WebElement Tolead = locateElement("xpath","//button[text()='Find Leads']");
	//click(Tolead);
	WebElement LeadId1 = locateElement("xpath","//input[@name='id']");
	type(LeadId1, "10215");

	WebElement FindLeads1 = locateElement("xpath","//button[text()='Find Leads']");
	click(FindLeads1);
	Thread.sleep(3000);
	
	WebElement FirstEle = locateElement("xpath","((//div[text()='Lead ID']//following::tr)[1]/td)[1]");
	String text=FirstEle.getText();
	System.out.println(text);
	click(FirstEle);
	
	switchToWindow(0);
	
	WebElement Merge = locateElement("linktext","Merge");
	Merge.click();
	
	
	Thread.sleep(1000);
	acceptAlert();
	
	String line1 = locateElement("xpath","//div[@class='messages']/div").getText();
	String line2 = locateElement("xpath","(//div[@class='messages']/div//following::li)[1]").getText();
	String line3 = locateElement("xpath","(//div[@class='messages']/div//following::li)[2]").getText();
	System.out.println(line1+"\n"+line2+"\n"+line3);
	
	WebElement Findleads = locateElement("linktext","Find Leads");
	click(Findleads);
	
	Thread.sleep(3000);
	WebElement LeadId2 = locateElement("xpath","//input[@name='id']");
	type(LeadId2, "108");
	

	WebElement FindLeads2 = locateElement("xpath","//button[text()='Find Leads']");
	click(FindLeads2);
	System.out.println();


		}
	}



