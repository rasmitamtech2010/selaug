package testcase;

import java.io.IOException;
import wdMethods.ProjectMethods;



import org.openqa.selenium.WebElement;


import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import utils.ReadExcel;


public class TC002_CreateLead extends ProjectMethods {
	@BeforeTest(groups="smoke")
	public void beforeTest()
	{
		testCaseName="TC002_Create Lead";
		testCaseDesc="To Create A lead";
		author="Abi";
		category="Smoke";
		System.out.println("before test");
	}
	
	@DataProvider(name="positive",indices= {0,2})
	public Object[][] fetch() throws IOException
	{
		return ReadExcel.readExcelData();
	}

	@Test(groups="smoke",dataProvider="positive" )
	public void CreateLead(String CName,String FName,String LName,String phno,String address) throws InterruptedException, IOException {
	Thread.sleep(3000);
	WebElement Lead = locateElement("linktext","Leads");
	click(Lead);
	Thread.sleep(3000);
	WebElement CreateLead = locateElement("linktext","Create Lead");
	click(CreateLead);
	WebElement company = locateElement("createLeadForm_companyName");
	type(company, CName);
	
	WebElement FirstName = locateElement("createLeadForm_firstName");
	type(FirstName, FName);
	WebElement LastName = locateElement("createLeadForm_lastName");
	type(LastName, LName);
	WebElement FrstLocName = locateElement("createLeadForm_firstNameLocal");
	type(FrstLocName, "Abi");
	WebElement LastLocName = locateElement("createLeadForm_lastNameLocal");
	type(LastLocName, "M");
	WebElement PersonalTittle = locateElement("createLeadForm_personalTitle");
	type(PersonalTittle, "Welcome");
	WebElement DatasrcId = locateElement("createLeadForm_dataSourceId");
	selectDropDownUsingText(DatasrcId,"Direct Mail");
	WebElement ProfTittle = locateElement("createLeadForm_generalProfTitle");
	type(ProfTittle, "Selenium");
	WebElement Revenue = locateElement("createLeadForm_annualRevenue");
	type(Revenue, "3,00,000");
	WebElement industryEnumId = locateElement("createLeadForm_industryEnumId");
	selectDropDownUsingText(industryEnumId,"Computer Software");
	WebElement OwnerShip = locateElement("createLeadForm_ownershipEnumId");
	selectDropDownUsingText(OwnerShip,"Public Corporation");
	WebElement SicCode = locateElement("createLeadForm_sicCode");
	type(SicCode,"99");
	WebElement Desc = locateElement("createLeadForm_description");
	type(Desc,"createLeadForm_description");
	WebElement ImpNote = locateElement("createLeadForm_importantNote");
	type(ImpNote,"createLeadForm_importantNote");
	WebElement CountryCode = locateElement("createLeadForm_primaryPhoneCountryCode");
	type(CountryCode,"IN");
	
	WebElement AreaCode = locateElement("createLeadForm_primaryPhoneAreaCode");
	type(AreaCode,"641104");
	
	WebElement PriPhone = locateElement("createLeadForm_primaryPhoneNumber");
	PriPhone.clear();
	type(PriPhone,phno);

	WebElement Extension = locateElement("createLeadForm_primaryPhoneExtension");
	type(Extension, "376");
	
	WebElement ccy = locateElement("createLeadForm_currencyUomId");
	selectDropDownUsingText(ccy, "INR - Indian Rupee");
	
	WebElement emp = locateElement("createLeadForm_numberEmployees");
	type(emp, "150");
	WebElement ticker = locateElement("createLeadForm_tickerSymbol");
	type(ticker, "$");
	WebElement PPAname = locateElement("createLeadForm_primaryPhoneAskForName");
	type(PPAname, "ABI");
	
	WebElement url = locateElement("createLeadForm_primaryWebUrl");
	type(url, "www.facebook.com");
	
	WebElement ToName = locateElement("createLeadForm_generalToName");
	type(ToName, "Kala");
	WebElement Add1 = locateElement("createLeadForm_generalAddress1");
	type(Add1, "8/89-c,Ram nagar Duluxe");
	
	WebElement Add2 = locateElement("createLeadForm_generalAddress2");
	type(Add2, address);
	WebElement city = locateElement("createLeadForm_generalCity");
	type(city, "Coimbatote");
	WebElement State = locateElement("createLeadForm_generalStateProvinceGeoId");
	selectDropDownUsingText(State, "Indiana");
	
	WebElement ctry = locateElement("createLeadForm_generalCountryGeoId");
	selectDropDownUsingText(ctry, "India");
	
	WebElement Postalcode = locateElement("createLeadForm_generalPostalCode");
	type(Postalcode, "641104");
	WebElement Zipcode = locateElement("createLeadForm_generalPostalCodeExt");
	type(Zipcode, "+91");
	
	WebElement MarketCamp = locateElement("createLeadForm_marketingCampaignId");
	selectDropDownUsingText(MarketCamp, "Demo Marketing Campaign");
	
	WebElement email = locateElement("createLeadForm_primaryEmail");
	type(email, "Abirami003@abi.net");
	
	
	
	WebElement Submit = locateElement("xpath","//input[@class='smallSubmit']");
	click(Submit);
	
	WebElement verifyname = locateElement("viewLead_firstName_sp");
	//String firstname=verifyname.getText();
	verifyExactText(verifyname, "Abirami");
		}
		

	
	

}