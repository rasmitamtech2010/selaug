package week1.day2;

public class FiboUsingDoWhile {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Printing Fibonacci Sequence");
		int fib1 = 0;
		int fib2 = 1;
		int temp = 0;
		System.out.println(fib1);
		System.out.println(fib2);
		do {
			temp = fib1 + fib2;
			System.out.println(temp);
			fib1 = fib2; //Replace 2nd with first number
			fib2 = temp; //Replace temp number with 2nd number
		} while (fib2 < 100);

	}

}
